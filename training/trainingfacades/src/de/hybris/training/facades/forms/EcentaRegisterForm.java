package de.hybris.training.facades.forms;

public class EcentaRegisterForm {

    private String titleCode;
    private String firstName;
    private String lastName;
    private String email;
    private String pwd;
    private String checkPwd;
    private EcentaConsentForm ecentaConsentForm;
    private boolean termsCheck;
    private String phoneNumber;

    public String getTitleCode() {
        return titleCode;
    }

    public void setTitleCode(String titleCode) {
        this.titleCode = titleCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getCheckPwd() {
        return checkPwd;
    }

    public void setCheckPwd(String checkPwd) {
        this.checkPwd = checkPwd;
    }

    public EcentaConsentForm getEcentaConsentForm() {
        return ecentaConsentForm;
    }

    public void setEcentaConsentForm(EcentaConsentForm ecentaConsentForm) {
        this.ecentaConsentForm = ecentaConsentForm;
    }

    public boolean isTermsCheck() {
        return termsCheck;
    }

    public void setTermsCheck(boolean termsCheck) {
        this.termsCheck = termsCheck;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
