<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="banner__component simple-banner">
    <img title="${fn:escapeXml(media.altText)}" alt="${fn:escapeXml(media.altText)}"
         src="${fn:escapeXml(media.url)}">
    <a href="${pdf.url}" class="pdf-button-on pdf-button">PDF ${name}</a>
   <div>${name}</div>
    <div>${info}</div>
    <div>${currency}</div>
</div>