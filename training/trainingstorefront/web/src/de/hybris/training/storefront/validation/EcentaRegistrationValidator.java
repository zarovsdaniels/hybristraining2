package de.hybris.training.storefront.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;
import de.hybris.training.facades.forms.EcentaRegisterForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component("ecentaRegistrationValidator")
public class EcentaRegistrationValidator extends RegistrationValidator implements Validator {
    @Override
    public boolean supports(final Class<?> aClass)
    {
        return EcentaRegisterForm.class.equals(aClass);
    }

    @Override
    public void validate(final Object object, final Errors errors)
    {
        final EcentaRegisterForm ecentaRegisterForm = (EcentaRegisterForm) object;
        final String titleCode = ecentaRegisterForm.getTitleCode();
        final String firstName = ecentaRegisterForm.getFirstName();
        final String lastName = ecentaRegisterForm.getLastName();
        final String phoneNumber = ecentaRegisterForm.getPhoneNumber();
        final String email = ecentaRegisterForm.getEmail();
        final String pwd = ecentaRegisterForm.getPwd();
        final String checkPwd = ecentaRegisterForm.getCheckPwd();
        final boolean termsCheck = ecentaRegisterForm.isTermsCheck();

        validateTitleCode(errors, titleCode);
        validateName(errors, firstName, "firstName", "register.firstName.invalid");
        validateName(errors, lastName, "lastName", "register.lastName.invalid");

        if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
        {
            errors.rejectValue("lastName", "register.name.invalid");
            errors.rejectValue("firstName", "register.name.invalid");
        }

        validateEmail(errors, email);
        validatePhoneNumber(errors, phoneNumber);
        validatePassword(errors, pwd);
        comparePasswords(errors, pwd, checkPwd);
        validateTermsAndConditions(errors, termsCheck);
    }

    protected void validatePhoneNumber(final Errors errors, final String phoneNumber)
    {
        if (StringUtils.isEmpty(phoneNumber))
        {
            errors.rejectValue("phoneNumber", "register.phoneNumber.invalid");
        }
        else if (StringUtils.length(phoneNumber) > 255)
        {
            errors.rejectValue("phoneNumber", "register.phoneNumber.invalid");
        }
    }
}
